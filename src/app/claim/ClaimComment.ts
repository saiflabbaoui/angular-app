import {Personne} from './Personne';
import {Claim} from './Claim';
import DateTimeFormat = Intl.DateTimeFormat;

export class ClaimComment {
  idComment: number;
  continu: String;
  personne: Personne;
  claim: Claim;
  dateComment: DateTimeFormat;

}
