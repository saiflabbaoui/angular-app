import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../router.animations';
import {Claim} from '../claim/Claim';
import {ClaimService} from '../claim.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
  animations: [routerTransition()]
})
export class ChartsComponent implements OnInit {
  claims: Claim[];

  tab1: any[];
  tab2: any[];
  tab3: any[];
  tab4: any[];
  tab5: any[];
  tab6: any[];
  tab7: any[];

  // bar chart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  public barChartType: string;
  public barChartLegend: boolean;
  public barChartData: any[] = [];

  // Doughnut
  public doughnutChartLabels: string[] = [
    'TECHNIQUE',
    'FINANCIERE',
    'RELATIONNELLE'
  ];
  public doughnutChartData: number[] ;
  public doughnutChartType: string;

  public radarChartType: string;

  public pieChartData: number[] = [300, 500, 100];
  public pieChartType: string;

  // PolarArea
  public polarAreaChartLabels: string[] = [
    '1',
    '2',
    '3',
    '4',
    '5'
  ];
  public polarAreaChartData: number[] = [300, 500, 100, 40, 120];
  public polarAreaLegend: boolean;

  public polarAreaChartType: string;

  // lineChart
  public lineChartData: Array<any> = [];
  public lineChartLabels: Array<any> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean;
  public lineChartType: string;

  constructor(private claimService: ClaimService, private router: Router) {
  }

  // events
  public chartClicked(e: any): void {
    // console.log(e);
  };

  public chartHovered(e: any): void {
    // console.log(e);
  };

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.random() * 100,
      56,
      Math.random() * 100,
      40
    ];
    const clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }

  ngOnInit() {
    this.getAllClaim();

    console.log(this.barChartData);
    this.barChartType = 'bar';
    this.barChartLegend = true;
    this.doughnutChartType = 'doughnut';
    this.radarChartType = 'radar';
    this.pieChartType = 'pie';
    this.polarAreaLegend = true;
    this.polarAreaChartType = 'polarArea';
    this.lineChartLegend = true;
    this.lineChartType = 'line';

  }

  getAllClaim() {

    this.claimService.getAllClaims()
      .subscribe(
        response => {
          this.claims = response;
          console.log(this.claims);
          console.log('message', this.claims.filter(x => new Date(x.date).getMonth() == 1).length);
          console.log('message', this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.type === 'FINANCIERE').length);
          this.tab1 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.type === 'TECHNIQUE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.type === 'TECHNIQUE').length];
          this.tab2 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.type === 'RELATIONNELLE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.type === 'RELATIONNELLE').length];
          this.tab3 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.type === 'FINANCIERE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.type === 'FINANCIERE').length];
          this.tab4 = [
            (this.claims.length/1000)*this.claims.filter(x =>  x.type === 'TECHNIQUE').length,
            (this.claims.length/1000)*this.claims.filter(x =>  x.type === 'FINANCIERE').length,
            (this.claims.length/1000)*this.claims.filter(x =>  x.type === 'RELATIONNELLE').length];
          console.log(this.tab1);
          this.barChartData = [
            {data: this.tab1, label: 'TECHNIQUE'},
            {data: this.tab2, label: 'RELATIONNELLE'},
            {data: this.tab3, label: 'FINANCIERE'},
          ];
          this.doughnutChartData=this.tab4;

          this.tab5 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.etat === 'FERMEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.etat === 'FERMEE').length];
          this.tab6 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.etat === 'EN_COURS').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.etat === 'EN_COURS').length];
          this.tab7 = [
            this.claims.filter(x => new Date(x.date).getMonth() == 1 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 2 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 3 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 4 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 5 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 6 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 7 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 8 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 9 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 10 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 11 && x.etat === 'TRAITEE').length,
            this.claims.filter(x => new Date(x.date).getMonth() == 12 && x.etat === 'TRAITEE').length];

          this.lineChartData=[
            {data: this.tab4 ,label: 'FERMEE'},
            {data: this.tab5, label: 'EN_COURS'},
            {data: this.tab6, label: 'TRAITEE'},
          ];
          console.log(this.barChartData);
        },
        error => {
          console.log(error);
        }
      );
  }

  countTypeClaimByMonth() {

    return this.tab1 = [
      this.claims.filter(x => new Date(x.date).getMonth() == 1).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 2).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 3).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 4).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 5).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 6).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 7).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 8).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 9).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 10).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 11).length,
      this.claims.filter(x => new Date(x.date).getMonth() == 12).length];
  }
}
