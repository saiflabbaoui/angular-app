import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimDetailFrontComponent } from './claim-detail-front.component';

describe('ClaimDetailFrontComponent', () => {
  let component: ClaimDetailFrontComponent;
  let fixture: ComponentFixture<ClaimDetailFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimDetailFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimDetailFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
