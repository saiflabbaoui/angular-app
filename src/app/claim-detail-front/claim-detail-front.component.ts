import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ClaimService} from '../claim.service';
import {ActivatedRoute} from '@angular/router';
import {Claim} from '../claim/Claim';
import {Personne} from '../claim/Personne';
import {ClaimComment} from '../claim/ClaimComment';
import {MatDialog} from '@angular/material';
import {Location} from '@angular/common';
import {ReviewPopupComponent} from '../Global/ReviewPopup/ReviewPopup.component';

@Component({
  selector: 'app-claim-detail-front',
  templateUrl: './claim-detail-front.component.html',
  styleUrls: ['./claim-detail-front.component.css']
})
export class ClaimDetailFrontComponent implements OnInit {
  personne: Personne;
  show: boolean = false;
  comment = new ClaimComment();
  claim: any;
  c: Claim;
  id: number;
  counterDateTime = new Date(new Date().setHours(24, 0, 0, 0));
  claims: Claim[];
  ClaimCommentComponent: any;
  Tags: any[];
  @ViewChild('name') commentInput: ElementRef;
  private readonly dialog: MatDialog;

  constructor(private claimService: ClaimService, private route: ActivatedRoute, private matdialog: MatDialog, location: Location) {
    this.id = this.route.snapshot.params['id'];
    this.dialog = matdialog;


  }

  ngOnInit() {
    this.claimService.getClaimById(this.id).subscribe(data => {
      this.claim = data;

      this.c = this.claim;
      this.Tags = this.c.etiquette.split('-');

      console.log(this.Tags);
      console.log(data);
    });
    this.getAllClaim();
  }

  getAllClaim() {
    this.claimService.getAllClaims()
      .subscribe(
        response => {
          console.log('hello');
          this.claims = response;
          console.log(this.claims);
          this.claims = this.claims.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
        },
        error => {
          console.log(error);
        }
      );
  }

  getClaimsByTag(s) {
    this.claimService.getClaimsByTag(s.toString())
      .subscribe(
        response => {
          this.claims = response;

          console.log(this.claims);
        },
        error => {
          console.log(error);
        }
      );
  }

  addComment(comment: ClaimComment) {
    if (comment.continu != null) {
      this.claimService.addComment(this.id, comment).subscribe(data => console.log('ok'));

      this.claim.push(comment);
    }


  }

  load(): void {
    window.location.reload();
  }


  focusComment(): void {
    this.claimService.manageClaimSendMail(this.id, this.claim).subscribe(data => console.log('ok'));

    this.commentInput.nativeElement.focus();

  }
}
