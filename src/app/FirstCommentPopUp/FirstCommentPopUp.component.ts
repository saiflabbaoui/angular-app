import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'FirstCommentPopUp',
  templateUrl: './FirstCommentPopUp.component.html',
  styleUrls: ['./FirstCommentPopUp.component.scss']
})
export class FirstCommentPopUpComponent implements OnInit {

   singleProductDetails : any;
   reviews : any;

   constructor(public dialogRef: MatDialogRef<FirstCommentPopUpComponent>) { }

   ngOnInit() {
   }

}
